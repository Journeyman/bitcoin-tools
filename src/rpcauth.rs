/*
This file is part of the bitcoin-tools package.

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
*/

use std::error::Error;
use std::io::Write;
use std::process;
use rand::{thread_rng, Rng};
use sha2::Sha256;
use hmac::{Hmac, Mac};
use clap::Parser;
use hmac::digest::FixedOutput;
use regex::Regex;

#[derive(Clone, Debug)]
pub struct RpcAuth
{
    pub username: String,
    pub password: String,
    pub salt: String,
}

impl RpcAuth
{
    pub fn username(&self) -> &str
    {
        return &self.username;
    }

    pub fn password(&self) -> &str
    {
        return &self.password;
    }

    pub fn new(username: &str, password: &str, salt: &str) -> Self
    {
        let mut my_salt = Self::generate_salt();

        if !salt.is_empty()
        {
            my_salt = String::from(salt);
        }

        RpcAuth
        {
            username: String::from(username),
            password: String::from(password),
            salt: my_salt,
        }
    }

    fn generate_salt() -> String
    {
        let mut buffer = [0u8; 16];
        thread_rng().fill(&mut buffer[..]);

        return hex::encode(buffer);
    }

    fn encode_password(&self) -> Result<String, Box<dyn Error>>
    {
        let mut mac = Hmac::<Sha256>::new_from_slice(self.salt.as_bytes())?;
        mac.write(self.password.as_bytes().as_ref())?;

        let result = mac.finalize_fixed().to_vec();

        return Ok(hex::encode(result));
    }

    pub fn encode(&self) -> Result<String, Box<dyn Error>>
    {
        let ec = self.encode_password()?;
        return Ok(format!("{}:{}${}", self.username, self.salt, ec));
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// The RPC username
    #[arg(short, long)]
    username: String,

    /// The RPC password
    #[arg(short, long)]
    password: String,

    /// Verifies the username and password parameter against the given authentication cookie.
    #[arg(short, long)]
    verify: Option<String>,

    /// If provided, the command will output <token>=<username>:<encoded password>
    #[arg(short, long)]
    token: Option<String>,
}

fn main()
{
    let args = Args::parse();
    if args.username.is_empty()
    {
        eprintln!("Error: username cannot be an empty string.");
        process::exit(1);
    }
    if args.password.is_empty()
    {
        eprintln!("Error: password cannot be an empty string.");
        process::exit(1);
    }
    if let Some(cookie) = args.verify
    {
        let salt = extract_salt(&cookie).unwrap_or_default();
        if salt.is_empty()
        {
            eprintln!("Error: invalid authentication cookie.");
            process::exit(1);
        }
        let auth_param = RpcAuth::new(&*args.username, &*args.password, &salt);
        let eupw = auth_param.encode().unwrap_or_default();
        if cookie == eupw
        {
            println!("Username and password match!");
            process::exit(0);
        }
        else
        {
            println!("Username and password do not match!");
            process::exit(1);
        }
    }

    let auth_param = RpcAuth::new(&*args.username, &*args.password, "");
    match auth_param.encode()
    {
        Ok(eupw) =>
        {

            if args.token.is_none()
            {
                println!("{}", eupw);
            }
            else
            {
                println!("{}={}", args.token.unwrap(), eupw);
            }
        }
        Err(e) =>
        {
            eprintln!("Error: failed to encode username and password ({}).", e.to_string());
        }
    }
}

fn extract_salt(cookie: &str) -> Option<String>
{
    let re = Regex::new("([^:\\s]+):(?<salt>[a-z0-9]{32})\\$[a-z0-9]{64}").expect("Error in regular expression (should not happen).");
    if let Some(caps) = re.captures(cookie)
    {
        return Some(String::from(&caps["salt"]));
    }

    return None;
}

