/*
This file is part of the bitcoin-tools package.

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
*/

use std::fmt::{Debug, Display};
use std::fs::File;
use std::io::{Read, Seek};
use std::process;
use std::path::{Path, PathBuf};
use std::process::Command;
use num_bigint::BigUint;
use regex::Regex;
use clap::Parser;
use cryptocoin_address::bitcoin::LegacyBitcoinAddress;
use cryptocoin_address::CryptocoinAddress;
use notify::{Config, RecommendedWatcher, RecursiveMode, Watcher};
use splitty::split_unquoted_char;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// The path of the FOUND.txt file.
    #[arg(short, long)]
    file: String,

    /// A string to be printed for each key found. $1 and $2 will be replaced with the WIF private key and payment address respectively.
    #[arg(short, long)]
    print: Option<String>,

    /// A command to be executed for each key found. $1 and $2 will be replaced with the WIF private key and payment address respectively.
    #[arg(short, long)]
    exec: Option<String>,

    /// Prints additional information on errors.
    #[arg(short, long)]
    verbose: bool,

    /// Indicates that the command should block and continue to scan the file until it is signalled to terminate.
    #[arg(short, long)]
    scan: bool,

    /// Indicates that a key should be rejected if its computed public key hash does not match the value in the file.
    #[arg(short = 't', long)]
    strict: bool,
}

fn main()
{
    let args = Args::parse();
    let scan_mode = args.scan;

    // Get the file from the command line or search the current directory.
    let input_path = PathBuf::from(&args.file);

    // If the file does not exist and scan mode has been requested, create an empty one for the watcher to watch.
    if !input_path.exists()
    {
        if scan_mode
        {
            let _f = File::create(&input_path).expect("Error creating placeholder file!");
        }
        else
        {
            eprintln!("Fatal error: the input file '{}' does not exist or is not readable.", input_path.to_str().unwrap());
        }
    }

    // Open the file
    let mut found_file = File::open(&input_path).expect("Error opening the target file!");

    if scan_mode
    {
        match scan_for_and_emit_keys(&mut found_file, &input_path, &args)
        {
            Ok(_) => {},
            Err(e) =>
            {
                eprintln!("Error reading keys from file: {}", e.to_string());
                process::exit(1);
            },
        }
    }
    else
    {
        match read_and_emit_keys(&mut found_file, &args)
        {
            Ok(_) => {},
            Err(e) =>
            {
                eprintln!("Error reading keys from file: {}", e.to_string());
                process::exit(1);
            },
        }
    }
}

fn perform_search(expression: &Regex, buffer: &String, arguments: &Args) -> Result<(), Box<dyn std::error::Error>>
{
    for cap in expression.captures_iter(&buffer)
    {
        let pkhash = &cap[1];
        let priv_key = BigUint::parse_bytes(&cap[3].as_bytes(), 16).unwrap();

        let legacy_address = match &cap[2]
        {
            "u" => LegacyBitcoinAddress::from_big_uint(&priv_key, false)?,
            "c" => LegacyBitcoinAddress::from_big_uint(&priv_key, true)?,
            _ =>
            {
                return Err("Error parsing data from file!")?;
            }
        };

        if arguments.strict && (legacy_address.public_key_hash() != pkhash)
        {
            if arguments.verbose
            {
                println!("Failed integrity check: {}", &cap[0]);
            }
            continue;
        }

        if let Some(p) = &arguments.print
        {
            let format = p.replace("$1", &*legacy_address.wif_private_key()).replace("$2", &*legacy_address.to_string());
            if !format.is_empty()
            {
                println!("{}", format);
            }
        }

        if let Some(e) = &arguments.exec
        {
            let exec = e.replace("$1", &*legacy_address.wif_private_key()).replace("$2", &*legacy_address.to_string());
            if !exec.is_empty()
            {
                let mut token = split_unquoted_char(&*exec, ' ').unwrap_quotes(true);

                let mut command = Command::new(token.next().expect("Empty command (should not happen)!"));
                while let Some(a) = token.next()
                {
                    command.arg(a);
                }

                let result = command.output()?;
                if arguments.verbose
                {
                    if let Ok(s) = String::from_utf8(result.stdout)
                    {
                        println!("Command succeeded and returned output:");
                        println!();
                        println!("{}", s);
                        println!();
                    }
                    else
                    {
                        println!("The command succeeded but did not produce any printable output.")
                    }
                }
            }
        }
    }
    return Ok(());
}

fn read_and_emit_keys(file: &mut File, arguments: &Args) -> Result<(), Box<dyn std::error::Error>>
{
    let re_legacy = Regex::new("([a-z0-9]{40}):([u|c]):\\(hex\\)priv:([a-z0-9]{64})")?;
    let re = Regex::new("([a-z0-9]{40}):([u|c]):priv:([a-z0-9]{64})\\s*\\+\\s*0x([a-z0-9]{2,64})")?;
    let mut buffer = String::new();

    let count = file.read_to_string(&mut buffer)?;

    if count > 0
    {
        perform_search(&re_legacy, &buffer, &arguments)?;
        perform_search(&re, &buffer, &arguments)?;
    }
    else
    {
        return Err("No keys in file!")?;
    }

    return Ok(());
}

fn scan_for_and_emit_keys(file: &mut File, file_path: &Path, arguments: &Args) -> Result<(), Box<dyn std::error::Error>>
{
    // Get the length of the file.
    let mut pos = 0;
    let mut size = file.metadata()?.len();
    let re_legacy = Regex::new("([a-z0-9]{40}):([u|c]):\\(hex\\)priv:([a-z0-9]{64})")?;
    let re = Regex::new("([a-z0-9]{40}):([u|c]):priv:([a-z0-9]{64})\\s*\\+\\s*0x([a-z0-9]{2,64})")?;

    // Set up watcher
    let (tx, rx) = std::sync::mpsc::channel();
    let mut watcher = RecommendedWatcher::new(tx, Config::default())?;
    watcher.watch(file_path.as_ref(), RecursiveMode::NonRecursive)?;

    // Watch the file.
    for res in rx
    {
        let mut buffer = String::new();
        match res
        {
            Ok(_event) =>
            {
                // Continue if the size of the file has not changed.
                if size == file.metadata()?.len()
                {
                    continue;
                }

                // Read from pos to end of file
                file.seek(std::io::SeekFrom::Start(pos))?;

                let count = file.read_to_string(&mut buffer)?;

                if count > 0
                {
                    perform_search(&re_legacy, &buffer, &arguments)?;
                    perform_search(&re, &buffer, &arguments)?;
                }

                // Update pos to end of file
                pos = file.seek(std::io::SeekFrom::Current(0))? - 3;
                // Update size to the new size of the file.
                size = file.metadata()?.len();
            }
            Err(e) =>
            {
                return Err(format!("Error in watcher event loop {}", e))?;
            },
        }
    }

    return Ok(());
}
