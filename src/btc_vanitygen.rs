/*
This file is part of the bitcoin-tools package.

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.
*/

use std::process;
use std::sync::{Arc, Mutex};
use clap::Parser;
use cryptocoin_address::CryptocoinAddress;
use cryptocoin_address::bitcoin::{Bech32SegWitBitcoinAddress, LegacyBitcoinAddress, LegacySegWitBitcoinAddress, TaprootBitcoinAddress};
use rand::RngCore;
use rand_chacha::ChaCha20Rng;
use rand_chacha::rand_core::SeedableRng;
use threadpool::ThreadPool;

#[derive(PartialEq, Copy, Clone)]
enum StringPosition
{
    None,
    Start,
    Within,
    End,
}

#[derive(PartialEq, Copy, Clone)]
enum Addresstype
{
    Legacy,
    SegWitP2SH,
    SegWitBech32,
    Taproot,
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args
{
    /// The type of address that should be generated, one of legacy, p2sh, bech32 or bech32m.
    #[arg(short, long = "address-type")]
    address_type: Option<String>,

    /// The vanity text that the address should contain.
    #[arg(short, long = "vanity-text")]
    vanity_text: Option<String>,

    /// The position within the address that the vanity text should occur.
    #[arg(short, long)]
    position: Option<String>,

    /// The maximum number of threads that should be used to search for candidate addresses.
    #[arg(short, long)]
    threads: Option<usize>,

    /// The maximum number of addresses that should be generated.
    #[arg(short, long)]
    max: Option<usize>,

    /// Forces the generation of uncompressed addresses (ignored for any address type other than 'legacy').
    #[arg(short, long)]
    uncompressed: bool,

    /// A format string that will be used to output the generated address and key. The token %a will be replaced with the address, %k will be replaced with the private key and %d will be replaced with a descriptor for the private key.
    #[arg(short = 'f', long = "format")]
    format: Option<String>,

    /// The merkle root for creating Taproot addresses.
    #[arg(short = 'r', long = "merkle-root")]
    merkle_root: Option<String>,
}

fn main()
{
    // Get the parameters.
    let args = Args::parse();
    let address_type;
    if let Some(s) = args.address_type
    {
        address_type = match s.to_lowercase().as_str()
        {
            "legacy" => Addresstype::Legacy,
            "p2sh" => Addresstype::SegWitP2SH,
            "bech32" => Addresstype::SegWitBech32,
            "bech32m" => Addresstype::Taproot,
            _ => { eprintln!("Error: invalid address type, should be one of legacy, p2sh, bech32 or bech32m."); process::exit(1); }
        }
    }
    else
    {
        address_type = Addresstype::SegWitBech32;
    }
    let vanity_text = match args.vanity_text
    {
        Some(s) => String::from(s.trim()),
        _ => String::new(),
    };

    let charset: &str;
    if address_type == Addresstype::Legacy || address_type == Addresstype::SegWitP2SH
    {
        charset = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
    }
    else
    {
        charset = "qpzry9x8gf2tvdw0s3jn54khce6mua7l";
    }
    for pc in vanity_text.chars()
    {
        if !charset.contains(pc)
        {
            println!("Error: invalid character in vanity text.");
            process::exit(1);
        }
    }

    let merkle_root: String;
    if address_type == Addresstype::Taproot
    {
        merkle_root = match args.merkle_root
        {
            Some(hs) => hs.clone().to_lowercase(),
            None => String::new(),
        };

        if !validate_merkle_root(merkle_root.as_str())
        {
            println!("Error: invalid merkle root string.");
            process::exit(1);
        }
    }
    else
    {
        merkle_root = String::new();
    }

    let position: StringPosition;
    if !vanity_text.is_empty()
    {
        if let Some(s) = args.position
        {
            position = match s.to_lowercase().as_str()
            {
                "start" => StringPosition::Start,
                "within" => StringPosition::Within,
                "end" => StringPosition::End,
                _ => { eprintln!("Error: invalid string position, should be one of start, within or end."); process::exit(1); },
            }
        }
        else
        {
            position = StringPosition::Within; // Default if none specified.
        }
    }
    else
    {
        position = StringPosition::None;
    }

    let threads = match args.threads
    {
        Some(n) => n,
        _ => num_cpus::get(),
    };

    let max_keys = match args.max
    {
        Some(n) => n,
        _ => 0,
    };
    
    let uncompressed = args.uncompressed;
    
    let format_string = args.format.unwrap_or_else(|| String::from("Address:     %a\nPrivate key: %k\nDescriptor:  %d"));

    let stop_flag = Arc::new(Mutex::new(false));
    let found_count = Arc::new(Mutex::new(0usize));
    let pool = ThreadPool::new(threads.try_into().unwrap());
    let thread_count = Arc::new(Mutex::new(0u64));

    loop
    {
        let pool = pool.clone();
        let thread_count = thread_count.clone();
        let stop_flag = stop_flag.clone();
        let stop_flag_outer = stop_flag.clone();
        let found_count = found_count.clone();
        let mr = merkle_root.clone();
        let vs = vanity_text.clone();
        let fs = format_string.clone();
        pool.execute(move ||
        {
            let mut thread_count = thread_count.lock().unwrap();
            *thread_count = *thread_count + 1;
            let mut rng = ChaCha20Rng::seed_from_u64(*thread_count);
            rng.set_stream(*thread_count);
            drop(thread_count);
            let mut found_count = found_count.lock().unwrap();
            let mut stop_flag = stop_flag.lock().unwrap();
            if *stop_flag
            {
                return;
            }
            if max_keys > 0 && *found_count >= max_keys
            {
                *stop_flag = true;
                return;
            }
            //let mut rng = rand::thread_rng();
            let mut buff: [u8; 32] = [0; 32];
            rng.try_fill_bytes(&mut buff).unwrap();
            match address_type
            {
                Addresstype::Legacy =>
                {
                    let address = LegacyBitcoinAddress::from_slice(&buff, !uncompressed).unwrap();
                    if check_and_print_address(&address, address_type, position, vs.as_str(), fs.as_str())
                    {
                        *found_count = *found_count + 1;
                    }
                },
                Addresstype::SegWitP2SH =>
                {
                    let address = LegacySegWitBitcoinAddress::from_slice(&buff).unwrap();
                    if check_and_print_address(&address, address_type, position, vs.as_str(), fs.as_str())
                    {
                        *found_count = *found_count + 1;
                    }
                },
                Addresstype::SegWitBech32 =>
                { 
                    let address = Bech32SegWitBitcoinAddress::from_slice(&buff).unwrap();
                    if check_and_print_address(&address, address_type, position, vs.as_str(), fs.as_str())
                    {
                        *found_count = *found_count + 1;
                    }
                },
                Addresstype::Taproot =>
                {
                    let address = TaprootBitcoinAddress::from_slice(&buff, mr.as_str()).unwrap();
                    if check_and_print_address(&address, address_type, position, vs.as_str(), fs.as_str())
                    {
                        *found_count = *found_count + 1;
                    }
                },
            };
        });

        let stop_flag = stop_flag_outer.lock().unwrap();
        if *stop_flag
        {
            break;
        }
    }

    pool.join();
}

fn check_and_print_address<A: CryptocoinAddress>(address: &A, address_type: Addresstype, position: StringPosition, vanity_string: &str, format_string: &str) -> bool
{
    if position != StringPosition::None && !vanity_string.is_empty()
    {
        let start_index: usize = match address_type
        {
            Addresstype::Legacy | Addresstype::SegWitP2SH => 1,
            Addresstype::SegWitBech32 | Addresstype::Taproot => 2,
        };
        
        let slice = &address.to_string()[start_index..];

        if position == StringPosition::Start && !slice.starts_with(vanity_string)
        {
            return false;
        }

        if position == StringPosition::Within && !slice.contains(vanity_string)
        {
            return false;
        }

        if position == StringPosition::End && !slice.ends_with(vanity_string)
        {
            return false;
        }
    }

    let mut echo_str = String::from(format_string);
    echo_str = echo_str.replace("%a", address.to_string().as_str());
    echo_str = echo_str.replace("%k", address.wif_private_key());
    echo_str = echo_str.replace("%d", address.descriptor());
    
    println!("{}", echo_str);
    
    return true;
}

fn validate_merkle_root(merkle_root: &str) -> bool
{
    if merkle_root.is_empty()
    {
        return true;
    }
    if (merkle_root.len() % 32) != 0 || merkle_root.len() > 128
    {
        return false;
    }

    let charset = "0123456789abcdef";
    for pc in merkle_root.chars()
    {
        if !charset.contains(pc)
        {
            return false;
        }
    }

    return true;
}