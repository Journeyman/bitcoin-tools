# bitcoin-tools
[![License](http://img.shields.io/badge/license-GPLV3-blue.svg)](https://gitlab.com/Journeyman/bitcoin-tools/-/blob/main/LICENSE)
[![pipeline status](https://gitlab.com/Journeyman/bitcoin-tools/badges/main/pipeline.svg)](https://gitlab.com/Journeyman/bitcoin-tools/-/commits/main)

## Description
A collection of command line utilities for performing useful or unusual tasks related to cryptocurrency.

## Installation
```shell
cargo install bitcoin-tools --git https://gitlab.com/Journeyman/bitcoin-tools`
```

## Usage
See the help for individual commands

## Contributing
Use the issue tracker to report problems, suggestions and questions. You may also contribute by submitting pull requests.

If you find this project helpful, please consider making a donation to my coffee fund: `bc1qq3u0pyvdk3zaj3aut0emqv3edaqwam2n2v9f03`

## Copyright
Copyright (C) 2023 - 2024 Chris Morrison

## License
See [LICENSE](https://gitlab.com/Journeyman/bitcoin-tools/-/blob/main/LICENSE)

